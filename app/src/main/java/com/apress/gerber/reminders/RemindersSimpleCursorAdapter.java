package com.apress.gerber.reminders;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;

public class RemindersSimpleCursorAdapter extends SimpleCursorAdapter {
  RemindersSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
    super(context, layout, c, from, to, flags);
  }

  // aby użyć viewholder, należy przesłonić dwie poniższe metody i zdefiniować klasę ViewHolder
  @Override
  public void bindView(View view, Context context, Cursor cursor) {
    super.bindView(view, context, cursor);
    ViewHolder holder = (ViewHolder) view.getTag();
    if (holder == null) {
      holder = new ViewHolder();
      holder.colImp = cursor.getColumnIndexOrThrow(RemindersDbAdapter.COL_IMORTANT);
      holder.listTab = view.findViewById(R.id.row_tab);
      view.setTag(holder);
    }
    if (cursor.getInt(holder.colImp) > 0) {
      holder.listTab.setBackgroundColor(
        context.getResources().getColor(R.color.orange)
      );
    } else {
      holder.listTab.setBackgroundColor(
        context.getResources().getColor(R.color.green)
      );
    }
  }

  @Override
  public View newView(Context context, Cursor cursor, ViewGroup parent) {
    return super.newView(context, cursor, parent);
  }

  static class ViewHolder {
    // zapamiętanie indeksu kolumny
    int colImp;
    // zapamiętanie widoku
    View listTab;
  }
}
