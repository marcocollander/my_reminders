package com.apress.gerber.reminders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class RemindersDbAdapter {
  // nazwy kolumn
  private static final String COL_ID = "_id";
  static final String COL_CONTENT = "content";
  static  final String COL_IMORTANT = "important";

  // dotyczące ich indeksy
  private static  final int INDEX_ID = 0;
  private static final int INDEX_CONTENT = INDEX_ID + 1;
  private static  final int INDEX_IMORTANT = INDEX_ID + 2;

  // używane w dzienniku zdarzeń
  private static final String TAG = "RemindersDbAdapter";
  private DatabaseHelper mDbHelper;
  private SQLiteDatabase mDb;
  private static final String DATABASE_NAME = "dba_remdrs";
  private static final String TABLE_NAME = "tbl_remdrs";
  private static final int DATABASE_VERSION = 1;
  private final Context mCtx;

  // polecenie SQL służące do utworzenia bazy ddanych
  private static final String DATABASE_CREATE =
    "CREATE TABLE if not exists " + TABLE_NAME + " ( " +
      COL_ID + " INTEGER PRIMARY KEY autoincrement, " +
      COL_CONTENT + " TEXT, " +
      COL_IMORTANT + " INTEGER );";

  RemindersDbAdapter(Context ctx){
    mCtx = ctx;
  }

  // otwarcie
  void open() throws SQLException {
    mDbHelper = new DatabaseHelper(mCtx);
    mDb = mDbHelper.getWritableDatabase();
  }

  // zamknięcie
  public void close(){
    if(mDbHelper != null){
      mDbHelper.close();
    }
  }

  //TWORZENIE
  // id zostanie utworzony automatycznie
  void createReminder(String name, boolean important){
    ContentValues values = new ContentValues();
    values.put(COL_CONTENT, name);
    values.put(COL_IMORTANT, important ? 1 : 0);
    mDb.insert(TABLE_NAME, null, values);
  }

  // przeciążenie pobierające obiekt przypomnienia
  public long createReminder(Reminder reminder){
    ContentValues values = new ContentValues();
    values.put(COL_CONTENT, reminder.getContent()); // treść
    values.put(COL_IMORTANT, reminder.getImportant()); // ważność

    // wstawianie wiersza
    return mDb.insert(TABLE_NAME, null, values);
  }

  //ODCZYT
  public Reminder fetchReminderById(int id){
    Cursor cursor = mDb.query(TABLE_NAME,
      new String[] {COL_ID, COL_CONTENT, COL_IMORTANT}, COL_ID + "=?",
      new String[]{String.valueOf(id)}, null, null, null, null);
    if(cursor != null)
      cursor.moveToFirst();
    return new Reminder(
      cursor.getInt(INDEX_ID),
      cursor.getString(INDEX_CONTENT),
      cursor.getInt(INDEX_IMORTANT)
    );
  }

  Cursor fetchAllReminders(){
    Cursor mCursor = mDb.query(TABLE_NAME, new String[]{COL_ID, COL_CONTENT, COL_IMORTANT},
      null,null, null, null, null);
    if(mCursor != null){
      mCursor.moveToFirst();
    }
    return mCursor;
  }

  // AKTUALIZACJA
  public void updateReminder(Reminder reminder){
    ContentValues values = new ContentValues();
    values.put(COL_CONTENT, reminder.getContent());
    values.put(COL_IMORTANT, reminder.getImportant());
    mDb.update(TABLE_NAME, values, COL_ID + "=?",
      new String[]{String.valueOf(reminder.getId())});
  }

  //USUNIĘCIE
  public void deleteReminderById(int nId){
    mDb.delete(TABLE_NAME,COL_ID +"=?",
      new String[]{String.valueOf(nId)});
  }

  void deleteAllReminders(){
    mDb.delete(TABLE_NAME, null, null);
  }


 // Klasa do obsługi niskopoziomowego API SQLite(otwarcie i zamknięcie bazy danych)
 // Używa obiektu typu Context - klasa abstrakcyjna dająca dostęp do
 // do systemu operacyjnego Android
  private static class DatabaseHelper extends SQLiteOpenHelper {
    DatabaseHelper(Context context){
      super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Metody wywołań zwrotnych wywoływane przez system operacyjny w trakcie życia aplikacji
    @Override
    public void onCreate(SQLiteDatabase db){
      Log.w(TAG, DATABASE_CREATE);
      db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      Log.w(TAG, "Aktualizacja bazy danych z wersji " + oldVersion + " do wersji " + newVersion + ". co powoduje wyczyszczenie zawartości bazy danych.");
      db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
      onCreate(db);
    }
  }
}