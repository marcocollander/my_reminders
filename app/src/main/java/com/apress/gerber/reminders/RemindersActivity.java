package com.apress.gerber.reminders;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class RemindersActivity extends AppCompatActivity {
  private RemindersSimpleCursorAdapter mCursorAdapter;
  private RemindersDbAdapter mDbAdapter;

  @Override
  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_reminders);

    ActionBar actionBar = getSupportActionBar();
    actionBar.setHomeButtonEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    actionBar.setIcon(R.mipmap.ic_launcher);

    ListView listView = findViewById(R.id.reminders_list_view);
    listView.setDivider(null);

    mDbAdapter = new RemindersDbAdapter(this);
    mDbAdapter.open();

    if (savedInstanceState == null) {
      // wyczyść wszystkie dane
     // mDbAdapter.deleteAllReminders();
      // dodaj przykładowe dane
      //insertSomeReminders(mDbAdapter);
    }

    Cursor cursor = mDbAdapter.fetchAllReminders();
    // z kolumnn zdefiniowanych w bazie danych
    String[] from = new String[]{RemindersDbAdapter.COL_CONTENT};
    // do identyfikatorów widoków w ukladzie graficznym
    int[] to = new int[]{R.id.row_text};
    mCursorAdapter = new RemindersSimpleCursorAdapter(
      RemindersActivity.this,
      R.layout.reminders_row,
      cursor,
      from,
      to,
      0
    );

    // cursorAdapter(kontroler) aktualizuje ListView(widok)
    // danymi z bazy danych(model)
    listView.setAdapter(mCursorAdapter);
    // gdy klikamy konkretny element ListView
    listView.setOnItemClickListener((parent, view, masterListPosition, id) -> {
      AlertDialog.Builder builder = new AlertDialog.Builder(RemindersActivity.this);
      ListView modeListView = new ListView(RemindersActivity.this);
      String[] modes = new String[]{"Edycja przypomnienia", "Usunięcie przypomnienia"};
      ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(RemindersActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, modes);
      modeListView.setAdapter(modeAdapter);
      builder.setView(modeListView);
      final Dialog dialog = builder.create();
      dialog.show();

      modeListView.setOnItemClickListener((parent1, view1, position, id1) -> {
        // edycja przypomnienia
        if (position == 0) {
          int nId = getIdFromPosition(masterListPosition);
          Reminder reminder = mDbAdapter.fetchReminderById(nId);
          fireCustomDialog(reminder);

          // usuniencie przypomnienia
        } else {
          mDbAdapter.deleteReminderById(getIdFromPosition(masterListPosition));
          mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
        }
        dialog.dismiss();
      });
    });

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
      listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
      listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
          MenuInflater inflater = mode.getMenuInflater();
          inflater.inflate(R.menu.cam_menu, menu);
          return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
          return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
          switch (item.getItemId()) {
            case R.id.menu_item_delete_reminder:
              for (int nC = mCursorAdapter.getCount() - 1; nC >= 0; nC--) {
                if (listView.isItemChecked(nC)) {
                  mDbAdapter.deleteReminderById(getIdFromPosition(nC));
                }
              }
              mode.finish();
              mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
              return true;
          }
          return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
      });
    }

    // Obiekt arrayAdapter jest w systemie MVC(model-widok-kontroler) kontrolerem
    /*
    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
      this,
      R.layout.reminders_row,          // układ graficzny (widok)
      R.id.row_text,                   // wiersz (widok)
      new String[]{                    // dane (model) z testowymi danymi przekazywanymi do ListView
        "pierwszy wiersz", "drugi wiersz", "trzeci wiersz", "czwarty wiersz"
      }
    );

    listView.setAdapter(arrayAdapter);

     */
  }

  private int getIdFromPosition(int nC) {
    return (int)mCursorAdapter.getItemId(nC);
  }

  private void insertSomeReminders(RemindersDbAdapter dbAdapter) {
    dbAdapter.createReminder("Kupić książkę", true);
    dbAdapter.createReminder("Wysłać prezent ojcu", false);
    dbAdapter.createReminder("Piątkowy obiad ze znajomymi", false);
    dbAdapter.createReminder("Gra w squasha", false);
    dbAdapter.createReminder("Odgarnąć i posolić podjazd", false);
    dbAdapter.createReminder("Przygotow ać program zajęć z Androida", true);
    dbAdapter.createReminder("Kupić nowe krzesło do biura", false);
    dbAdapter.createReminder("Zadzwonić do mechanika", false);
    dbAdapter.createReminder("Odnowić członkostwo w klubie", false);
    dbAdapter.createReminder("Kupić nowy telefon Android Galaxy", true);
    dbAdapter.createReminder("Sprzedać stary telefon Android - aukcja", false);
    dbAdapter.createReminder("Kupić nowe wiosła do kajaka", false);
    dbAdapter.createReminder("Zadzwonić do księgowego", false);
    dbAdapter.createReminder("Kupić 300 000 akcji Google", false);
    dbAdapter.createReminder("Oddzwonić do Dalajlamy", true);
  }

  private void fireCustomDialog(final Reminder reminder){
    // własne okno dialogowe
    final Dialog dialog = new Dialog(this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.dialog_custom);
    TextView titleView = dialog.findViewById(R.id.custom_title);
    final EditText editCustom = dialog.findViewById(R.id.custom_edit_reminder);
    Button commitButton = dialog.findViewById(R.id.custom_button_commit);
    final CheckBox checkBox = dialog.findViewById(R.id.custom_check_box);
    LinearLayout rootLayout = dialog.findViewById(R.id.custom_root_layout);
    final boolean isEditOperation = (reminder != null);
    // dotyczy edycji
    if(isEditOperation){
      titleView.setText("Edycja przypomnienia");
      checkBox.setChecked(reminder.getImportant() == 1);
      editCustom.setText(reminder.getContent());
      rootLayout.setBackgroundColor(getResources().getColor(R.color.blue));
    }

    commitButton.setOnClickListener(v -> {
      String reminderText = editCustom.getText().toString();
      if(isEditOperation) {
        Reminder reminderEdited = new Reminder(reminder.getId(), reminderText, checkBox.isChecked() ? 1 : 0);
        mDbAdapter.updateReminder(reminderEdited);
        // nowe przypomnienie
      }else{
        mDbAdapter.createReminder(reminderText, checkBox.isChecked());
      }
      mCursorAdapter.changeCursor(mDbAdapter.fetchAllReminders());
      dialog.dismiss();
    });

    Button buttonCancel = dialog.findViewById(R.id.custom_button_cancel);
    buttonCancel.setOnClickListener(v -> dialog.dismiss());

    dialog.show();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_reminders, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_new:
        // utworzenie nowego przypomnienia
        fireCustomDialog(null);
        return true;
      case R.id.action_exit:
        finish();
      default:
        return false;
    }
  }
}
