package com.apress.gerber.reminders;

/**
 * Model danych
 */
public class Reminder {
  private int mId;
  private String mContent;
  private int mImportant;

  Reminder(int id, String content, int important) {
    mId = id;
    mContent = content;
    mImportant = important;
  }

  int getId() {
    return mId;
  }

  public void setId(int id) {
    mId = id;
  }

  String getContent() {
    return mContent;
  }

  public void setContent(String content) {
    mContent = content;
  }

  int getImportant() {
    return mImportant;
  }

  public void setImportant(int important) {
    mImportant = important;
  }
}
